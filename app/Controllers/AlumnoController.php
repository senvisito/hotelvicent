<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\AlumnoModel; //decimos donde está

use Config\Services;

/**
 * Description of AlumnoController
 *
 * @author jose
 */
class AlumnoController extends BaseController {

    public function index(){
        $alumnoModel = new AlumnoModel();
        /*echo '<pre>';
        print_r($alumnoModel->findAll());
        echo '</pre>';*/
        $data['title'] = 'Listado de Alumnado';
        $data['alumnos'] = $alumnoModel->findAll();
        return view('alumno/lista',$data);
    }
    
    /*
    public function formInsertAlumno(){
        $data['title'] = 'Formulario Alta Alumnos';
        return view('alumno/formalta', $data);
    }
    
    //este método será llamado tras rellenar los campos del formulario y enviarlo.
    public function insertAlumno(){
        //Tomar los datos del formulario
        $NIA = $this->request->getPost('NIA'); //es el valor del atributo name del campo input
        $nombre = $this->request->getPost('nombre');
        $apellido1 = $this->request->getPost('apellido1');
        $apellido2 = $this->request->getPost('apellido2');
        $nif = $this->request->getPost('nif');
        $email = $this->request->getPost('email');
        //lo pasamos a un array asociativo
        $alumno_nuevo = [
            'NIA' => $NIA,
            'nombre' => $nombre,
            'apellido1' => $apellido1,
            'apellido2' => $apellido2,
            'nif' => $nif,
            'email' => $email,
        ];
        //comprobamos lo que hemos recibido del formulario
        echo '<pre>';
        print_r($alumno_nuevo);
        echo '</pre>';
        //Si trabajamos con la base de datos necesitamos un modelo.
        //creamos el modelo que nos interesa
        $alumnoModel = new AlumnoModel();
        //usamos el método insert del modelo para añadir los datos de un alumno
        $alumnoModel->insert($alumno_nuevo);
        //Ahora queremos que siga añadiendo nuevos alumnos
        return redirect()->to('alumno/formalta');
    }
*/    
    public function inserta(){
        $data['title'] = 'Formulario Alta Alumnos';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('alumno/formalta', $data); 
        } else {
            $alumno_nuevo = [
            'NIA' => $this->request->getPost('NIA'),
            'nombre' => $this->request->getPost('nombre'),
            'apellido1' => $this->request->getPost('apellido1'),
            'apellido2' => $this->request->getPost('apellido2'),
            'nif' => $this->request->getPost('nif'),
            'email' => $this->request->getPost('email'),
        ];
            $alumnoModel = new AlumnoModel();
            if ($alumnoModel->insert($alumno_nuevo) === false){
               $data['errores'] = $alumnoModel->errors(); 
               return view('alumno/formalta', $data);  
            }
            
        }
    }
    
    
    /*
    public function formEdit($id){
        helper('form');
        $alumnoModel = new AlumnoModel();
        $data['alumno'] = $alumnoModel->find($id);
        $data['titulo'] = 'Modificar Alumno/a';
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        return view('alumno/formEdit',$data);
    }
    */
    
    public function actualiza($id){
        helper('form');
        $data['title'] = 'Modificar Alumno/a';
        $alumnoModel = new AlumnoModel();
        $data['alumno'] = $alumnoModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('alumno/formEdit',$data);
        } else {
            $alumno = $this->request->getPost();
            unset($alumno['botoncito']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
            $alumnoModel->setValidationRule('NIA', 'required|numeric|min_length[8]');
            if ($alumnoModel->update($id, $alumno) === false){
                //hay un error al actualizar
                $data['errores'] = $alumnoModel->errors();
                return view('alumno/formEdit',$data);
            }
        }        
        return redirect()->to('alumno/lista');
    }

    
}

