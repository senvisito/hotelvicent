<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\HotelModel; //decimos donde está

use Config\Services;
/**
 * Description of HotelController
 *
 * @author a023942078k
 */
class HotelController extends BaseController{
    //put your code here
    public function index(){
        $hotelModel = new HotelModel();
        /*echo '<pre>';
        print_r($hotelModel->findAll());
        echo '</pre>';*/
       $data['title'] = 'Listado de Hoteles';
        $data['hoteles'] = $hotelModel->findAll();
         return view('hotel/lista',$data);
    }
    
   public function inserta(){
        $data['title'] = 'Formulario Alta Hoteles';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { //la primera vez
           return view('hotel/formalta', $data); 
        } else {
            $hotel_nuevo = [
            'nombre' => $this->request->getPost('nombre'),
            'descripcion' => $this->request->getPost('descripcion'),
            'localidad' => $this->request->getPost('localidad'),
            'direccion' => $this->request->getPost('direccion'),
            'cp' => $this->request->getPost('cp'),
            'email' => $this->request->getPost('email'),
        ];
            $hotelModel = new HotelModel();
            if ($hotelModel->insert($hotel_nuevo) === false){
               $data['errores'] = $hotelModel->errors(); 
               return view('hotel/formalta', $data);  
            }
            
        }
    }
    
    public function actualiza($id){
        helper('form');
        $data['title'] = 'Modificar Hotel';
        $hotelModel = new HotelModel();
        $data['hotel'] = $hotelModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('hotel/formEdit',$data);
        } else {
            $hotel = $this->request->getPost();
            unset($hotel['botoncito']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
            $hotelModel->setValidationRule('nombre', 'required');
            if ($hotelModel->update($id, $hotel) === false){
                //hay un error al actualizar
                $data['errores'] = $hotelModel->errors();
                return view('hotel/formEdit',$data);
            }
        }        
        return redirect()->to('hotel/lista');
    }
    
    public function borra($id) {
        $hotelModel = new HotelModel();
        $hotelModel->delete($id);
        return redirect()->to('hotel/lista');
    }
    
}

