<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;


class HotelModel extends Model{ 
    protected $table = 'hoteles';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir que campos son modificables.
    protected $allowedFields = ['nombre','descripcion','localidad','direccion','cp','email'];
    protected $validationRules = [
        'nombre'    => 'required',
        'email'     => 'valid_email',
    ];
    
    
}