<?= $this->extend('templates/default_1') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('hotel/alta')?>">Insertar</a>
        </li>
    </ul>


    <table class="table table-striped" id="myTable">
        <thead>
            <th>
                nombre
            </th>
            <th>
                email
            </th>
            <th>
                Acciones
            </th>
        </thead>
        <tbody>
        <?php foreach ($hoteles as $hotel): ?>
            <tr>
                
                <td>
                    <?= $hotel->nombre ?> 
                </td>
                <td>
                    <?= $hotel->email ?>
                </td>
                
                <td class="text-right">
                    <a href="<?=site_url('hotel/actualiza/'.$hotel->id)?>" title="Editar <?= $hotel->nombre.' '.$hotel->email ?>">
                    <span class="bi bi-pencil-square"></span>
                    </a>
                
                    <a href="<?=site_url('hotel/borra/'.$hotel->id)?>" title="Borrar <?= $hotel->nombre.' '.$hotel->email ?>" onclick="return confirm('¿Estás seguro de borrar el hotel <?=$hotel->nombre?> ?')">
                    <i class="bi bi-eraser-fill"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>
<?= $this->endSection() ?>
