<?= $this->extend('templates/default_1') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('hotel/alta')?>" method="post">

        <div class="form-group">
            <?= form_label('nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('descripcion:', 'descripcion', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('descripcion',set_value('descripcion',''),['class'=>'form_control col-9', 'id'=>'descripcion']) ?>
        </div>
        <div class="form-group">
            <?= form_label('localidad:', 'localidad', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('localidad',set_value('localidad',''),['class'=>'form_control col-9', 'id'=>'localidad']) ?>
        </div>
        <div class="form-group">
            <?= form_label('direccion:', 'direccion', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('direccion',set_value('direccion',''),['class'=>'form_control col-9', 'id'=>'direccion']) ?>
        </div>
         <div class="form-group">
            <?= form_label('cp:', 'cp', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('cp',set_value('cp',''),['class'=>'form_control col-9', 'id'=>'cp']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Correo electrónico:', 'email', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('email',set_value('email','algo@correo.com'),['class'=>'form_control col-9', 'id'=>'email']) ?>
        </div>
        <input type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>
